package contracttestexample;

import java.util.Date;

public class Order {

	private int creationDate;

	public Order(int creationDate) {
		this.creationDate = creationDate;
	}

	public Date getCreationDate() {
		return new Date(creationDate);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + creationDate;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Order other = (Order) obj;
		if (creationDate != other.creationDate)
			return false;
		return true;
	}
}
