package contracttestexample;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class DatabaseImpl implements Database {
	
	private List<Order> orders = new ArrayList<Order>();

	@Override
	public void placeOrder(Order order) {
		orders .add(order);
	}

	@Override
	public List<Order> getOrdersPlacedSince(Date date) {
		List<Order> result = new ArrayList<Order>();
		
		for (Order order : orders) {
			if (order.getCreationDate().after(date)) {
				result.add(order);
			}
		}
		
		return result;
	}

}
