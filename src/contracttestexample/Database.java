package contracttestexample;

import java.util.Date;
import java.util.List;

public interface Database {

	public void placeOrder(final Order order);
	
	public List<Order> getOrdersPlacedSince(final Date date);
}
