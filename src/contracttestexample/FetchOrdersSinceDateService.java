package contracttestexample;

import java.util.Date;
import java.util.List;

public class FetchOrdersSinceDateService {

	private Database database;
	
	public FetchOrdersSinceDateService(final Database database) {
		this.database = database;
	}

	public String execute(final Date date) {
		List<Order> orders = database.getOrdersPlacedSince(date);
		
		if (orders.size() == 1) {
			return "render one result";
		}
		return null;
	}
}
