package contracttestexample;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Date;

public class ContractTests extends ContractsForFetchOrdersSinceDateServiceTest {

	@Override
	public void ifResultHasOneOrder_renderOneResult() {
		DatabaseImpl database = new DatabaseImpl();

		database.placeOrder(new Order(1));
		database.placeOrder(new Order(3));

		assertEquals(Arrays.asList(new Order(3)), database.getOrdersPlacedSince(new Date(1)));
	}
}
