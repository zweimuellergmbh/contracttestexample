package contracttestexample;

import org.junit.Test;


public abstract class ContractsForFetchOrdersSinceDateServiceTest {

	@Test
	public abstract void ifResultHasOneOrder_renderOneResult();
}