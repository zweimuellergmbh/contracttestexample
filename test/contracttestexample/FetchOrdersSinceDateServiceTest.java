package contracttestexample;

import java.util.Date;

import org.junit.Test;
import org.mockito.Mockito;

public class FetchOrdersSinceDateServiceTest {

	// check, that our client calls the correct method on the server
	@Test
	public void expect_getOrdersPlacedSince_calledOnServer() throws Exception {
		Database database = Mockito.mock(Database.class);
		FetchOrdersSinceDateService service = new FetchOrdersSinceDateService(database);
		
		Date date = new Date();
		
		service.execute(date);
		
		// set expectations
		Mockito.verify(database, Mockito.times(1)).getOrdersPlacedSince(date);
	}
}	
