package contracttestexample;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Date;

import org.mockito.Mockito;

public class CollaborationTests extends ContractsForFetchOrdersSinceDateServiceTest {

	public void ifResultHasOneOrder_renderOneResult() {
		Date date = new Date(3);
		
		Database database = Mockito.mock(Database.class);
		Mockito.stub(database.getOrdersPlacedSince(date)).toReturn(Arrays.asList(new Order(3)));
	
		FetchOrdersSinceDateService service = new FetchOrdersSinceDateService(database);
		
		assertEquals("render one result", service.execute(date));
	}

}
